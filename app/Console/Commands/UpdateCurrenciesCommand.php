<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


use App\Currency;
use Exception;
use Illuminate\Console\Command;
use Stichoza\GoogleTranslate\GoogleTranslate;


/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class UpdateCurrenciesCommand extends Command
{
    const CURRENCY_URL = 'https://www.cbr-xml-daily.ru/daily_json.js';

    protected $translate;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "update:currencies";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update all currencies";


    public function __construct()
    {
        parent::__construct();
        $this->translate = new GoogleTranslate();
        $this->translate->setSource('ru');
        $this->translate->setTarget('en');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', self::CURRENCY_URL);
            $statusCode = $response->getStatusCode();
            if($statusCode == 200) {
                $response = json_decode($response->getBody(), true);
                $currencies = $response['Valute'];
                foreach ($currencies as $currency) {
                    $model = Currency::findOrNew(['digit_code' => $currency['NumCode']]);
                    $model->name = $currency['Name'];
                    $model->english_name = $this->translate->translate($currency['Name']);
                    $model->alphabetic_code = $currency['CharCode'];
                    $model->digit_code = $currency['NumCode'];
                    $model->rate = $currency['Value'];
                    $model->save();
                }
                $this->info("All currencies have been updated");
            }else {
                $this->info("status code{$statusCode}");
            }
        } catch (Exception $e) {
            $this->error("An error occurred");
        }
    }
}
