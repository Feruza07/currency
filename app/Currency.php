<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    public static function findOrNew($arr) {
        $currency = self::where($arr)->first();
        return ($currency == null) ? new self() : $currency;
    }

}
