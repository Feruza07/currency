<?php

namespace App\Http\Controllers;

use App\Currency;

class CurrencyController extends Controller
{

    public function currencies() {
        $currencies = Currency::all();
        return $currencies;
    }

    public function currency($id) {
        $currency = Currency::find($id);
        return $currency;
    }


}
